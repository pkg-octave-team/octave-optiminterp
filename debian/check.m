files = {"example_optiminterp",
         "test_optiminterp",
         "test_optiminterp_mult"};

for i = 1 : length (files)
    disp (sprintf ("[%s]", files {i}));
    source (sprintf ("inst/%s.m", files {i}));
endfor
