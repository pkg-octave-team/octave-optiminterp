#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.
#

dnl autoconf 2.13 certainly doesn't work! What is the minimum requirement?
AC_PREREQ(2.2)

AC_INIT([octave optiminterp],[0.3.7])

dnl Kill caching --- this ought to be the default
define([AC_CACHE_LOAD], )dnl
define([AC_CACHE_SAVE], )dnl

dnl uncomment to put support files in another directory
dnl AC_CONFIG_AUX_DIR(admin)

dnl if mkoctfile doesn't work, then we need the following:
dnl AC_PROG_CXX
dnl AC_PROG_F77
AC_PROG_CC


dnl *******************************************************************
dnl Sort out mkoctfile version number and install paths

dnl XXX FIXME XXX latest octave has octave-config so we don't
dnl need to discover things here.  Doesn't have --exe-site-dir
dnl but defines --oct-site-dir and --m-site-dir

dnl Check for mkoctfile
AC_CHECK_PROG(MKOCTFILE,mkoctfile,mkoctfile)
test -z "$MKOCTFILE" &&	AC_MSG_WARN([no mkoctfile found on path])

dnl Check for octave-cofig
AC_CHECK_PROG(OCTAVE_CONFIG,octave-config,octave-config)
test -z "$OCTAVE_CONFIG" &&	AC_MSG_WARN([no octave-config found on path])

dnl *******************************************************************

dnl Fortran compiler flags
dnl Note remove "-pipe" from FFLAGS as gfortran v4.0.x has issue with
dnl this an F90 files
FFLAGS=`$MKOCTFILE -p FFLAGS | sed -e 's/-pipe//'`
AC_SUBST(FFLAGS)
dnl *******************************************************************

dnl Check for features of your version of mkoctfile.
dnl All checks should be designed so that the default
dnl action if the tests are not performed is to do whatever
dnl is appropriate for the most recent version of Octave.

dnl Define the following macro:
dnl    OF_CHECK_LIB(lib,fn,true,false,helpers)
dnl This is just like AC_CHECK_LIB, but it doesn't update LIBS
AC_DEFUN(OF_CHECK_LIB,
[save_LIBS="$LIBS"
AC_CHECK_LIB($1,$2,$3,$4,$5)
LIBS="$save_LIBS"
])

dnl Define the following macro:
dnl    TRY_MKOCTFILE(msg,program,action_if_true,action_if_false)
dnl
AC_DEFUN(TRY_MKOCTFILE,
[AC_MSG_CHECKING($1)
cat > conftest.cc << EOF
#include <octave/oct.h>
$2
EOF
ac_try="$MKOCTFILE -c conftest.cc"
if AC_TRY_EVAL(ac_try) ; then
   AC_MSG_RESULT(yes)
   $3
else
   AC_MSG_RESULT(no)
   $4
fi
])

dnl
dnl Check if F77_FUNC works with MKOCTFILE
dnl
TRY_MKOCTFILE([for F77_FUNC],
[int F77_FUNC (hello, HELLO) (const int &n);],,
[MKOCTFILE="$MKOCTFILE -DF77_FUNC=F77_FCN"])

dnl **********************************************************

dnl Evaluate an expression in octave
dnl
dnl OCTAVE_EVAL(expr,var) -> var=expr
dnl
AC_DEFUN(OCTAVE_EVAL,
[AC_MSG_CHECKING([for $1 in Octave])
$2=`echo "disp($1)" | $OCTAVE -qf`
AC_MSG_RESULT($$2)
AC_SUBST($2)
])

dnl Evaluate an expression in octave-config
dnl
dnl OCTAVE_CONFIG_EVAL(expr,var) -> var=expr
dnl
AC_DEFUN(OCTAVE_CONFIG_EVAL,
[AC_MSG_CHECKING([for $1 in octave-config])
$2=`$OCTAVE_CONFIG -p $1`
AC_MSG_RESULT($$2)
AC_SUBST($2)
])

dnl Check status of an octave variable
dnl
dnl OCTAVE_CHECK_EXIST(variable,action_if_true,action_if_false)
dnl
AC_DEFUN(OCTAVE_CHECK_EXIST,
[AC_MSG_CHECKING([for $1 in Octave])
if test `echo 'disp(exist("$1"))' | $OCTAVE -qf`X != 0X ; then
   AC_MSG_RESULT(yes)
   $2
else
   AC_MSG_RESULT(no)
   $3
fi
])

dnl should check that $(OCTAVE) --version matches $(MKOCTFILE) --version
AC_CHECK_PROG(OCTAVE,octave,octave)

OCTAVE_CONFIG_EVAL(VERSION,OCTAVE_VERSION)
dnl get major ver part as used for a HAVE_OCTAVE_XXX define
ver=`echo $OCTAVE_VERSION | sed -e "s/\.//" -e "s/\..*$//"`
AC_SUBST(ver)

AC_SUBST(MKOCTFILE_FORTRAN_90)
AC_SUBST(OPTIMINTERP_LIBS)
AC_SUBST(OPTIMINTERP_CFLAGS)

OPTIMINTERP_LIBS="$LDFLAGS"
OPTIMINTERP_CFLAGS="$CPPFLAGS"

dnl By default, autoconf tries to compile Fortran programs with
dnl optimization (-O2) which mkoctfile does not support as argument

FCFLAGS=

dnl Set Language to Fortran
AC_LANG(Fortran)

dnl Use mkoctfile as Fortran 90 compiler.
dnl No tests are performed at this stage if mkoctfile can actually handle
dnl Fortran 90 code.

AC_PROG_FC([mkoctfile], 1990)

dnl Test if mkoctfile and the Fortran 90 compiler support
dnl the extention .F90 (free-form with pre-processor directives)
export FFLAGS
AC_FC_SRCEXT(F90,,AC_ERROR([mkoctfile does not accept files with the extention .F90. \
Support for this file extension has been added to version 2.9.9 of octave.]))

dnl Test if mkoctfile can handle Fortran 90 features such as modules

AC_MSG_CHECKING([whether mkoctfile accepts Fortran 90])

AC_COMPILE_IFELSE([[\
module test_module; \
contains; \
subroutine foo()
end subroutine 
end module \
]], MKOCTFILE_FORTRAN_90=yes, MKOCTFILE_FORTRAN_90=no)

OPTIMINTERPSTATUS=$MKOCTFILE_FORTRAN_90
AC_MSG_RESULT([$MKOCTFILE_FORTRAN_90])

if test $MKOCTFILE_FORTRAN_90 = no ; then
    AC_MSG_ERROR([mkoctfile does not accept Fortran 90 code. Octave was not build with a Fortran 90 compiler.])
fi

dnl Delete the module file. Capitalization is compiler dependent
rm -f test_module.mod TEST_MODULE.mod test_module.MOD TEST_MODULE.MOD

# check if can use -fallow-argument-mismatch

AC_MSG_CHECKING([whether Fortran accepts option -fallow-argument-mismatch])
saved_FFLAGS=$FFLAGS
FFLAGS="$FFLAGS -fallow-argument-mismatch"
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [])],
                  [fallow_argument_mismatch=yes],
                  [fallow_argument_mismatch=no])
AC_MSG_RESULT([$fallow_argument_mismatch])
if test "x${fallow_argument_mismatch}" = xno ; then
   FFLAGS=$saved_FFLAGS
fi

AC_OUTPUT([Makefile])

AC_MSG_RESULT([
$PACKAGE_NAME is configured with
   octave:      $OCTAVE (version $OCTAVE_VERSION)
   mkoctfile:	$MKOCTFILE for Octave $OCTAVE_VERSION
   octave-config: $OCTAVE_CONFIG for Octave $OCTAVE_VERSION
   optiminterp toolbox: $OPTIMINTERPSTATUS
])

